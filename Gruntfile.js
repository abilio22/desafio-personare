module.exports = function(grunt) {
    grunt.initConfig({
        copy: {
        	desafio: {
        		expand:true,
        		cwd: 'dev',
        		src: '**',
        		dest: 'dist'
        	}
        },
        clean: ['dist'],
        watch: {
        	options: {
        		livereload: true
        	},
        	arquivos: {
        		files: ['dev/**/*']
        	},
        	css: {
				files: '**/*.scss',
				tasks: ['sass']
			}
        },
        connect: {
        	server: {
        		options: {
        			livereload: true,
        			port: '9000',     
					hostname: '*',    
					livereload: true, 
					base: {
						path: 'dev'
					}
        		}
        	}
        },
          sass: {
			dist: {
				files: {
					'dev/css/style.css' : 'dev/sass/style.scss'
				}
			}
  }
    });
    
    grunt.registerTask('renovar', ['clean', 'copy']);
    grunt.registerTask('servidor', ['connect', 'watch']);
    grunt.loadNpmTasks ('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-sass');
}