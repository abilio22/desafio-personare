var viewport = document.documentElement.clientWidth;
var menu = document.getElementById("menu");
var listaMenu = document.getElementById("lista-menu");

function menuMobile() {
	if (viewport >= 1024 ) {
		menu.className = "menu-fat";
	}
	else if (viewport <= 768) {
    	menu.className = "menu-mobile";
	}
}

function menuSlim() {
	if (viewport >=768){
	    if (document.body.scrollTop > 350 || document.documentElement.scrollTop > 350) {
	        menu.className = "menu-slim";
	    }

	    else if(document.body.scrollTop < 350 || document.documentElement.scrollTop < 350) {
	    	menu.className = "menu-fat";
	    }
	}
}

function toggleMenu() {
    listaMenu.classList.toggle("aberto");
}

window.onscroll = function() {menuSlim()};



menuMobile();

var owl = $("#owl-wrap").data('owlCarousel');

if( $('html').hasClass('no-backgroundsize') )
{
  $(window).load(function() {
    window.setTimeout(function() {
      owl.reinit();
    }, 250);
  });
}